import React from 'react';
import {
  FlatList,
  Image,
  ListRenderItem,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {dummyNewsStyles as styles} from './styles';

export default function DummyNews() {
  const _renderItem: ListRenderItem<(typeof dummyNews)[0]> = ({item}) => (
    <TouchableOpacity style={styles.newsItem}>
      <Image
        source={{uri: item.image}}
        resizeMode="cover"
        style={styles.imageNews}
      />
      <View style={styles.newsHeaderView}>
        <Text style={styles.newsTitle} numberOfLines={1}>
          {item.title}
        </Text>
        <Text numberOfLines={3} style={styles.newsSubtitle}>
          {item.description}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.newsMainView}>
      <Text style={styles.newsMainText}>Últimas noticias</Text>
      <View style={styles.newsListView}>
        <FlatList data={dummyNews} renderItem={_renderItem} />
      </View>
    </View>
  );
}

const dummyNews = [
  {
    key: 0,
    title: '¡Qué calor! ¿Por qué ha incrementado la temperatura últimamente?',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum est at placerat dignissim. Curabitur accumsan quam non sapien luctus, eget suscipit eros mollis. Nullam eget sollicitudin mauris, eu rutrum eros. Vivamus feugiat tristique arcu, non lacinia dui interdum sit amet. Proin blandit metus dui, at fermentum justo scelerisque quis. Duis laoreet justo ut luctus pellentesque. Mauris ut viverra tortor. Cras id odio at odio finibus finibus. Pellentesque sagittis bibendum dui eget aliquet. Nunc blandit malesuada maximus. Donec sit amet sodales libero. Cras tempus sem ac fringilla posuere. Praesent ipsum lorem, eleifend eget rutrum non, porta ultrices turpis. Aliquam iaculis, magna a dapibus luctus, arcu nulla dignissim odio, consequat bibendum purus tellus at mi. Aliquam erat volutpat.',
    image: 'https://images.pexels.com/photos/301599/pexels-photo-301599.jpeg',
  },
  {
    key: 1,
    title: 'Jalisco alcanza récords máximos',
    description: 'Lorem ipsum...',
    image:
      'https://images.unsplash.com/photo-1563657296610-d0459418a7a7?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  },
  {
    key: 2,
    title: 'Recomendaciones importantes para el clima caluroso',
    description: 'Lorem ipsum...',
    image:
      'https://images.pexels.com/photos/416528/pexels-photo-416528.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    key: 3,
    title: '¿Cómo prepararte para un viaje?',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum est at placerat dignissim. Curabitur accumsan quam non sapien luctus, eget suscipit eros mollis. Nullam eget sollicitudin mauris, eu rutrum eros. Vivamus feugiat tristique arcu, non lacinia dui interdum sit amet. Proin blandit metus dui, at fermentum justo scelerisque quis. Duis laoreet justo ut luctus pellentesque. Mauris ut viverra tortor. Cras id odio at odio finibus finibus. Pellentesque sagittis bibendum dui eget aliquet. Nunc blandit malesuada maximus. Donec sit amet sodales libero. Cras tempus sem ac fringilla posuere. Praesent ipsum lorem, eleifend eget rutrum non, porta ultrices turpis. Aliquam iaculis, magna a dapibus luctus, arcu nulla dignissim odio, consequat bibendum purus tellus at mi. Aliquam erat volutpat.',
    image:
      'https://images.unsplash.com/photo-1436491865332-7a61a109cc05?q=80&w=2074&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  },
  {
    key: 4,
    title: 'Mejores destinos para viajar este año',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum est at placerat dignissim. Curabitur accumsan quam non sapien luctus, eget suscipit eros mollis. Nullam eget sollicitudin mauris, eu rutrum eros. Vivamus feugiat tristique arcu, non lacinia dui interdum sit amet. Proin blandit metus dui, at fermentum justo scelerisque quis. Duis laoreet justo ut luctus pellentesque. Mauris ut viverra tortor. Cras id odio at odio finibus finibus. Pellentesque sagittis bibendum dui eget aliquet. Nunc blandit malesuada maximus. Donec sit amet sodales libero. Cras tempus sem ac fringilla posuere. Praesent ipsum lorem, eleifend eget rutrum non, porta ultrices turpis. Aliquam iaculis, magna a dapibus luctus, arcu nulla dignissim odio, consequat bibendum purus tellus at mi. Aliquam erat volutpat.',
    image:
      'https://images.unsplash.com/photo-1531141445733-14c2eb7d4c1f?q=80&w=1965&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  },
];
