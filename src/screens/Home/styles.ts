import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  rootView: {flex: 1, backgroundColor: 'black'},
  rootInnerView: {flex: 1, backgroundColor: 'white'},
  imageBgStyle: {
    backgroundColor: 'black',
  },
  absoluteBlackView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
    opacity: 0.5,
  },
  reservamosLogoStyle: {
    width: '100%',
    height: 20,
    resizeMode: 'contain',
    marginTop: 15,
  },
  weatherLogoText: {
    position: 'absolute',
    fontSize: 10,
    color: 'white',
    top: 30,
  },
  searcherView: {
    marginTop: 40,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  engagementText: {fontSize: 25, color: 'white', fontWeight: 'bold'},
  searcherInnerView: {width: '100%'},
  textInput: {
    height: 40,
    backgroundColor: 'white',
    margin: 10,
    alignSelf: 'stretch',
    padding: 10,
    borderRadius: 4,
    color: 'black',
  },
  deleteTextView: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  deleteTextImage: {width: 20, height: 20},
  notSearchingMainView: {margin: 10, flex: 1},
  infoView: {
    padding: 15,
    paddingHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
  },
  infoIcon: {width: 25, height: 25},
  infoText: {paddingLeft: 10, fontSize: 12, color: 'gray'},
});

export default styles;
