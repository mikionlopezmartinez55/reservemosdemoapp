import {format} from 'date-fns';
import {es} from 'date-fns/locale';

import {OPENAPIWEATHER_ICONS} from '../../constants';

export const capitalizeFirstLetter = (string: string) =>
  string.charAt(0).toUpperCase() + string.slice(1);

export const formatUnixDateToHumanReadable = (date: number) =>
  capitalizeFirstLetter(format(date * 1000, 'EEEE dd MMMM', {locale: es}));

export const formatIconURL = (icon: string) =>
  `${OPENAPIWEATHER_ICONS}/${icon}@2x.png`;
