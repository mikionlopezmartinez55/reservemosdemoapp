import {StyleSheet} from 'react-native';

export const dummyNewsStyles = StyleSheet.create({
  newsMainView: {marginTop: 15, flex: 1},
  newsMainText: {fontSize: 21, color: 'black'},
  newsListView: {width: '100%', marginTop: 10, marginBottom: 20},
  newsItem: {
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    marginBottom: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
  },
  imageNews: {width: 100, height: 100},
  newsHeaderView: {padding: 8, flex: 1, marginLeft: 4},
  newsTitle: {fontSize: 15, color: 'black'},
  newsSubtitle: {color: 'gray'},
});

export const resultListStyles = StyleSheet.create({
  rootView: {flex: 1, marginTop: 10},
  resultItemView: {
    borderWidth: 1,
    borderColor: 'gray',
    marginBottom: 8,
    marginHorizontal: 10,
    padding: 5,
    paddingHorizontal: 8,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: 'white',
  },
  resultTitle: {fontSize: 20, color: 'black'},
  resultSubtitle: {color: 'gray'},
});
