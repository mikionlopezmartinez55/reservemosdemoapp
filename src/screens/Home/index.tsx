import React, {useMemo, useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  useWindowDimensions,
  View,
} from 'react-native';
import debounce from 'lodash.debounce';

import {ReservamosAPIResult} from './interfaces';
import ResultsList from './components/ResultsList';
import DummyNews from './components/DummyNews';
import {RESERVAMOS_API} from '../../constants';
import styles from './styles';

export default function Home(): React.JSX.Element {
  const {width} = useWindowDimensions();

  const [input, setInput] = useState('');
  const [reservamosAPIData, setReservamosAPIData] = useState<
    ReservamosAPIResult[]
  >([]);

  // Avoid excesive calling of APIs by debouncing the user input
  const debouncedCallback = useMemo(
    () => debounce(inputText => fetchReservamosAPI(inputText), 500),
    [],
  );

  const fetchReservamosAPI = async (citySearched: string) => {
    try {
      // If the input is too small, don't search anything yet
      if (citySearched.length < 3) {
        return;
      }

      const response = await fetch(
        `${RESERVAMOS_API}/places?q=${citySearched}`,
      );
      const json: ReservamosAPIResult[] = await response.json();
      const filtered = json.filter(x => x.result_type === 'city');

      setReservamosAPIData(filtered);
    } catch (error) {
      console.error('ERROR:', error);
    }
  };

  const handleTextChange = (text: string) => {
    setInput(text);
    debouncedCallback(text);

    if (!text.length) {
      setReservamosAPIData([]);
    }
  };

  return (
    <SafeAreaView style={styles.rootView}>
      <View style={styles.rootInnerView}>
        <ImageBackground
          style={styles.imageBgStyle}
          resizeMode="cover"
          source={require('../../assets/images/mexicoCityBanner.jpg')}>
          <View style={styles.absoluteBlackView} />
          <View>
            <View>
              <Image
                source={require('../../assets/images/reservamosLogo.png')}
                style={styles.reservamosLogoStyle}
              />

              <Text style={[styles.weatherLogoText, {left: width * 0.5 + 18}]}>
                Weather
              </Text>
            </View>

            <View style={styles.searcherView}>
              <Text style={styles.engagementText}>
                ¿Cómo está el clima en...
              </Text>

              <View style={styles.searcherInnerView}>
                <TextInput
                  value={input}
                  onChangeText={handleTextChange}
                  placeholder="Escribe una ciudad"
                  placeholderTextColor={'gray'}
                  style={styles.textInput}
                />
                {Boolean(input.length) && (
                  <TouchableOpacity
                    style={styles.deleteTextView}
                    onPress={() => {
                      setInput('');
                      setReservamosAPIData([]);
                    }}>
                    <Image
                      source={require('../../assets/images/deleteIcon.png')}
                      style={styles.deleteTextImage}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </ImageBackground>

        {/* Show when there are API results */}
        {Boolean(reservamosAPIData.length) && (
          <ResultsList data={reservamosAPIData} />
        )}

        {/* Show when user hasn't searched anything */}
        {!reservamosAPIData.length && (
          <View style={styles.notSearchingMainView}>
            <View style={styles.infoView}>
              <Image
                source={require('../../assets/images/infoIcon.png')}
                style={styles.infoIcon}
              />
              <Text style={styles.infoText}>
                Ingresa la ciudad que quieres buscar, y selecciona el resultado
                que te interese, se abrirá una ventana para poder revisar el
                clima de la ciudad seleccionada
              </Text>
            </View>

            <DummyNews />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
}
