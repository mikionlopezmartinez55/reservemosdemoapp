import React from 'react';
import {
  FlatList,
  ListRenderItem,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {ReservamosAPIResult} from '../interfaces';
import {resultListStyles as styles} from './styles';

interface IProps {
  data: ReservamosAPIResult[];
}

export default function ResultsList({
  data,
}: Readonly<IProps>): React.JSX.Element {
  const navigation = useNavigation();

  const _renderItem: ListRenderItem<ReservamosAPIResult> = ({item}) => (
    <TouchableOpacity
      style={styles.resultItemView}
      onPress={() =>
        navigation.navigate('CityDetails', {
          cityName: `${item.city_name}, ${item.state}`,
          coordinates: {lat: item.lat, long: item.long},
        })
      }>
      <Text style={styles.resultTitle}>{item.display}</Text>

      <Text style={styles.resultSubtitle}>
        {item.state}, {item.country}
      </Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.rootView}>
      <FlatList data={data} renderItem={_renderItem} />
    </View>
  );
}
