import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, Image, ListRenderItem, Text, View} from 'react-native';

import {RootStackParamList} from '../../../App';
import {ActualWeather, DailyWeather, WeatherAPIData} from './interfaces';
import {
  capitalizeFirstLetter,
  formatIconURL,
  formatUnixDateToHumanReadable,
} from './commonFunctions';
import {OPENAPIWEATHER_API, OPENAPIWEATHER_APIKEY} from '../../constants';
import styles from './styles';

export default function CityDetails({
  route,
}: Readonly<
  NativeStackScreenProps<RootStackParamList, 'CityDetails'>
>): React.JSX.Element {
  const {
    coordinates: {lat, long},
  } = route.params;

  const [dailyWeather, setDailyWeather] = useState<DailyWeather[]>([]);
  const [actualWeather, setActualWeather] = useState<ActualWeather>();

  const fetchCityWeather = useCallback(async () => {
    try {
      const url = `${OPENAPIWEATHER_API}/onecall?lat=${lat}&lon=${long}&exclude=minutely,hourly,alerts&lang=es&units=metric&appid=${OPENAPIWEATHER_APIKEY}`;
      const response = await fetch(url);
      const json: WeatherAPIData = await response.json();

      formatWeatherData(json);
    } catch (error) {
      console.error('ERROR:', error);
    }
  }, [lat, long]);

  const formatWeatherData = (weatherData: WeatherAPIData) => {
    const formattedDailyData: DailyWeather[] = weatherData.daily.map(
      ({dt, weather, temp}) => {
        return {
          day: formatUnixDateToHumanReadable(dt),
          min: `${temp.min.toFixed(0)}°`,
          max: `${temp.max.toFixed(0)}°`,
          icon: formatIconURL(weather[0].icon),
        };
      },
    );

    const {
      current: {temp, dt, weather, feels_like, humidity},
    } = weatherData;

    const formattedActualData: ActualWeather = {
      currentTemp: `${temp.toFixed(0)}°`,
      day: formatUnixDateToHumanReadable(dt),
      icon: formatIconURL(weather[0].icon),
      feelsLike: `${feels_like.toFixed(0)}°`,
      weatherDescription: capitalizeFirstLetter(weather[0].description),
      humidity: `${humidity}%`,
    };

    setDailyWeather(formattedDailyData);
    setActualWeather(formattedActualData);
  };

  useEffect(() => {
    fetchCityWeather();
  }, [fetchCityWeather]);

  const _renderItem: ListRenderItem<DailyWeather> = ({
    item: {icon, day, min, max},
  }) => (
    <View style={styles.dayCard}>
      <Image
        source={{
          uri: icon,
        }}
        style={styles.dailyIcon}
      />

      <Text style={styles.dailyMainDay}>{day}</Text>
      <Text style={styles.dailyBoldText}>
        Max: <Text style={styles.dailyNotBoldText}>{max}</Text>
      </Text>
      <Text style={styles.dailyBoldText}>
        Min: <Text style={styles.dailyNotBoldText}>{min}</Text>
      </Text>
    </View>
  );

  return (
    <View>
      {actualWeather && dailyWeather.length ? (
        <>
          <View style={styles.actualWeatherMainView}>
            <Text style={styles.cardTitle}>Hoy</Text>
            <View style={styles.actualCardMainInnerView}>
              <Image
                source={{uri: actualWeather.icon}}
                style={styles.actualIcon}
              />

              <View>
                <Text style={styles.actualDayText}>{actualWeather.day}</Text>
                <Text style={styles.actualDayDescription}>
                  {actualWeather.weatherDescription}
                </Text>

                <View style={styles.actualMainInfoView}>
                  <View>
                    <Text style={styles.actualTextData}>
                      Temperatura:{' '}
                      <Text style={styles.actualTextValue}>
                        {actualWeather.currentTemp}
                      </Text>
                    </Text>
                    <Text style={styles.actualTextData}>
                      Sensación:{' '}
                      <Text style={styles.actualTextValue}>
                        {actualWeather.feelsLike}
                      </Text>
                    </Text>
                    <Text style={styles.actualTextData}>
                      Humedad:{' '}
                      <Text style={styles.actualTextValue}>
                        {actualWeather.humidity}
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.dailyCardMainView}>
            <Text style={styles.dailyMainText}>Siguientes 7 días</Text>
            <FlatList horizontal data={dailyWeather} renderItem={_renderItem} />
          </View>
        </>
      ) : (
        <View style={styles.loadingMainView}>
          <Text style={styles.loadingText}>Cargando...</Text>
        </View>
      )}
    </View>
  );
}
