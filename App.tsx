import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from './src/screens/Home';
import CityDetails from './src/screens/CityDetails';

const Stack = createNativeStackNavigator<RootStackParamList>();

// Define types of the screens and it's props here
export type RootStackParamList = {
  Home: undefined;
  CityDetails: {cityName: string; coordinates: {lat: string; long: string}};
};

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

function App(): React.JSX.Element {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CityDetails"
          component={CityDetails}
          options={({route}) => ({title: route.params.cityName})}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
